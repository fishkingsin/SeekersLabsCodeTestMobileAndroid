package hk.kingsin.seekerslabscodetestmobileandroid

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.Observer
import hk.kingsin.seekerslabscodetestmobileandroid.data.OperationCallback
import hk.kingsin.seekerslabscodetestmobileandroid.data.Rate
import hk.kingsin.seekerslabscodetestmobileandroid.model.RateDataSource
import hk.kingsin.seekerslabscodetestmobileandroid.model.RateRepository
import hk.kingsin.seekerslabscodetestmobileandroid.viewmodel.RatesViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.*
import org.mockito.Mockito.*
import kotlin.coroutines.CoroutineContext

class ViewModelUnitTests {
    @Mock
    private lateinit var rateDataSource: RateDataSource

    @Mock
    private lateinit var context: Application

    @Captor
    private lateinit var operationCallbackCaptor: ArgumentCaptor<OperationCallback<List<Rate>?>>

    private lateinit var viewModel: RatesViewModel
    private lateinit var repository: RateRepository

    private lateinit var isViewLoadingObserver: Observer<Boolean>
    private lateinit var onMessageErrorObserver: Observer<Any>
    private lateinit var emptyListObserver: Observer<Boolean>
    private lateinit var onRenderRatessObserver: Observer<List<Rate>?>

    private lateinit var rateEmptyList: List<Rate>
    private lateinit var rateList: List<Rate>

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        `when`(context.applicationContext).thenReturn(context)

        repository = RateRepository(rateDataSource)
        viewModel = RatesViewModel(repository)

        mockData()
        setupObservers()
    }

    @Test
    fun `retrieve rates with ViewModel and Repository returns empty data`() {
        with(viewModel) {
            _loadRates()
            isViewLoading.observeForever(isViewLoadingObserver)
            isEmptyList.observeForever(emptyListObserver)
            outputs.liveRate.observeForever(onRenderRatessObserver)
        }

        verify(rateDataSource, times(1)).fetchRates(Mockito.eq(emptyList()), capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onSuccess(rateEmptyList)

        Assert.assertNotNull(viewModel.isViewLoading.value)
        Assert.assertTrue(viewModel.isEmptyList.value == true)
        Assert.assertTrue(viewModel.outputs.liveRate.value?.size == 0)
    }

    @Test
    fun `retrieve rates with ViewModel and Repository returns full data`() {
        with(viewModel) {
            _loadRates()
            outputs.isViewLoading.observeForever(isViewLoadingObserver)
            outputs.liveRate.observeForever(onRenderRatessObserver)
        }

        verify(rateDataSource, times(1)).fetchRates(Mockito.eq(listOf()), capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onSuccess(rateList)

        Assert.assertNotNull(viewModel.outputs.isViewLoading.value)
        Assert.assertTrue(viewModel.outputs.liveRate.value?.size == 3)
    }

    @Test
    fun `retrieve rates with ViewModel and Repository returns an error`() {
        with(viewModel) {
            _loadRates()
            outputs.isViewLoading.observeForever(isViewLoadingObserver)
            outputs.onMessageError.observeForever(onMessageErrorObserver)
        }
        verify(rateDataSource, times(1)).fetchRates(Mockito.eq(emptyList()), capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onError("An error occurred")
        Assert.assertNotNull(viewModel.outputs.isViewLoading.value)
        Assert.assertNotNull(viewModel.outputs.onMessageError.value)
    }

    private fun setupObservers() {
        isViewLoadingObserver = mock(Observer::class.java) as Observer<Boolean>
        onMessageErrorObserver = mock(Observer::class.java) as Observer<Any>
        emptyListObserver = mock(Observer::class.java) as Observer<Boolean>
        onRenderRatessObserver = mock(Observer::class.java) as Observer<List<Rate>?>
    }

    private fun mockData() {
        rateEmptyList = emptyList()
        val mockList: MutableList<Rate> = mutableListOf()
        mockList.add(
            Rate("USDHKD",0.0,0,0.0,0.0,  0.0)
        )
        mockList.add(Rate("USDHKD",0.0,0,0.0,0.0,  0.0))
        mockList.add(Rate("USDHKD",0.0,0,0.0,0.0,  0.0))

        rateList = mockList.toList()
    }
}