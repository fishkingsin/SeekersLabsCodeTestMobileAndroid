package hk.kingsin.seekerslabscodetestmobileandroid.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import hk.kingsin.seekerslabscodetestmobileandroid.R
import hk.kingsin.seekerslabscodetestmobileandroid.databinding.FragmentHomeBinding
import hk.kingsin.seekerslabscodetestmobileandroid.databinding.LayoutEmptyBinding
import hk.kingsin.seekerslabscodetestmobileandroid.databinding.LayoutErrorBinding
import hk.kingsin.seekerslabscodetestmobileandroid.data.Rate
import hk.kingsin.seekerslabscodetestmobileandroid.di.Injection
import hk.kingsin.seekerslabscodetestmobileandroid.view.RateAdapter
import hk.kingsin.seekerslabscodetestmobileandroid.viewmodel.RatesViewModel

class HomeFragment : Fragment() {

    private lateinit var progressBar: ProgressBar
    private lateinit var layoutEmpty: LayoutEmptyBinding
    private lateinit var layoutError: LayoutErrorBinding
    private lateinit var viewModel: RatesViewModel
    private lateinit var adapter: RateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var dataBinding = FragmentHomeBinding.inflate(inflater, container, false )

        val recyclerView = dataBinding.recyclerView
        val rowHeader = dataBinding.rowHeader
        rowHeader.textViewName.text = getString(R.string.symbol)
        rowHeader.textViewVariationValue.text = getString(R.string.changed)
        rowHeader.textViewBuyValue.text = getString(R.string.buy)
        rowHeader.textViewSellValue.text = getString(R.string.sell)

        layoutError = dataBinding.layoutError
        layoutEmpty = dataBinding.layoutEmpty
        progressBar = dataBinding.progressBar
        adapter = RateAdapter(viewModel.outputs.liveRate.value ?: emptyList())
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        recyclerView.adapter = adapter

        return dataBinding.root
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(
            this,
            Injection.provideViewModelFactory()
        ).get(RatesViewModel::class.java)

        viewModel.outputs.liveRate.observe(this, renderRates)
        viewModel.outputs.isViewLoading.observe(this, isViewLoadingObserver)
        viewModel.outputs.onMessageError.observe(this, onMessageErrorObserver)
        viewModel.outputs.isEmptyList.observe(this, emptyListObserver)
        viewModel.inputs.startTimer()
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        viewModel.inputs.loadRates()
    }
    //observers
    private val renderRates = Observer<List<Rate>?> {
        layoutError.root.visibility = View.GONE
        layoutEmpty.root.visibility = View.GONE
        adapter.update(it)
    }

    private val isViewLoadingObserver = Observer<Boolean> {
        Log.v(TAG, "isViewLoading $it")
        val visibility = if (it) View.VISIBLE else View.GONE
        progressBar.visibility = visibility
    }

    private val onMessageErrorObserver = Observer<Any> {
        Log.v(TAG, "onMessageError $it")
        layoutError.root.visibility = View.VISIBLE
        layoutEmpty.root.visibility = View.GONE
        layoutError.textViewError.text = "Error $it"
    }

    private val emptyListObserver = Observer<Boolean> {
        Log.v(TAG, "emptyListObserver $it")
        layoutEmpty.root.visibility = View.VISIBLE
        layoutError.root.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadRates()
    }

    override fun onPause() {
        super.onPause()
        viewModel.cancel()
    }

    companion object {
        const val TAG = "CONSOLE"
    }
}