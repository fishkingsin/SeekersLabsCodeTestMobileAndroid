package hk.kingsin.seekerslabscodetestmobileandroid.di

import androidx.lifecycle.ViewModelProvider
import hk.kingsin.seekerslabscodetestmobileandroid.data.ApiClient
import hk.kingsin.seekerslabscodetestmobileandroid.data.RateRemoteDataSource
import hk.kingsin.seekerslabscodetestmobileandroid.model.RateDataSource
import hk.kingsin.seekerslabscodetestmobileandroid.model.RateRepository
import hk.kingsin.seekerslabscodetestmobileandroid.viewmodel.ViewModelFactory



object Injection {

    private val RATE_DATA_SOURCE: RateDataSource = RateRemoteDataSource(ApiClient)
    private val rateRepository = RateRepository(providerRepository())
    private val rateViewModelFactory = ViewModelFactory(rateRepository)

    private fun providerRepository(): RateDataSource {
        return RATE_DATA_SOURCE
    }

    fun provideViewModelFactory(): ViewModelProvider.Factory {
        return rateViewModelFactory
    }
}