package hk.kingsin.seekerslabscodetestmobileandroid.model

import hk.kingsin.seekerslabscodetestmobileandroid.data.OperationCallback
import hk.kingsin.seekerslabscodetestmobileandroid.data.Rate

interface RateDataSource {
    fun fetchRates(pairs: List<String>?, callback: OperationCallback<List<Rate>?>)
    fun cancel()
}