package hk.kingsin.seekerslabscodetestmobileandroid.model

import hk.kingsin.seekerslabscodetestmobileandroid.data.OperationCallback
import hk.kingsin.seekerslabscodetestmobileandroid.data.Rate

class RateRepository(private val dataSource: RateDataSource) {

    fun fetchRates(pairs: List<String>, callback: OperationCallback<List<Rate>?>) {
        dataSource.fetchRates(pairs, callback)
    }

    fun cancel() {
        dataSource.cancel()
    }
}