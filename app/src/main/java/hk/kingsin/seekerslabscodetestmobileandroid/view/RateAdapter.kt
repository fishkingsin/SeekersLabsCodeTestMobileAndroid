package hk.kingsin.seekerslabscodetestmobileandroid.view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import hk.kingsin.seekerslabscodetestmobileandroid.R
import hk.kingsin.seekerslabscodetestmobileandroid.data.Rate
import hk.kingsin.seekerslabscodetestmobileandroid.databinding.RowRateBinding

class RateAdapter(private var rates: List<Rate>?) :
    RecyclerView.Adapter<RateAdapter.RateItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RateItemViewHolder {
        val binding = RowRateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RateItemViewHolder(binding, parent.context.resources)

    }

    override fun onBindViewHolder(vh: RateItemViewHolder, position: Int) {
        rates?.let { rates ->
            rates[position].let {
                vh.bind(it)
            }
        }
    }

    override fun getItemCount(): Int {
        return (rates?.size ?: 0)
    }

    fun update(data: List<Rate>?) {
        rates = data
        notifyDataSetChanged()
    }

    open class RateItemViewHolder(var viewBinding: RowRateBinding, resources: Resources) : RecyclerView.ViewHolder(
        viewBinding.root
    ) {
        private val dataBinding: RowRateBinding = viewBinding

        private val textViewName: TextView = dataBinding.textViewName
        private val textViewVariationValue: TextView = dataBinding.textViewVariationValue
        private val textViewBuyValue: TextView = dataBinding.textViewBuyValue
        private val textViewSellValue: TextView = dataBinding.textViewSellValue
        private val resources = resources
        open fun bind(rate: Rate?) {

            textViewName.text = rate?.name?.capitalize()
            rate?.buyPrice?.let {
                textViewBuyValue.text = "%.3f".format(it)
            }
            rate?.sellPrice?.let {
                textViewSellValue.text = "%.3f".format(it)
            }
            rate?.variation?.let { variation ->
                textViewVariationValue.setTextColor(
                    getColor(variation)
                )
                textViewVariationValue.text = "%.3f".format(variation) + "%"
            }
        }

        private fun getColor(variation: Double): Int {
            return if (variation > 0) {
                ResourcesCompat.getColor(
                    resources,
                    R.color.green,
                    null
                )
            } else {
                ResourcesCompat.getColor(
                    resources,
                    R.color.red,
                    null
                )
            }
        }
    }
}