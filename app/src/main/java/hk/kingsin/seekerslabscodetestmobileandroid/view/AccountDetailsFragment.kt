package hk.kingsin.seekerslabscodetestmobileandroid.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import hk.kingsin.seekerslabscodetestmobileandroid.data.Rate
import hk.kingsin.seekerslabscodetestmobileandroid.databinding.AccountdetailFragmentBinding
import hk.kingsin.seekerslabscodetestmobileandroid.di.Injection
import hk.kingsin.seekerslabscodetestmobileandroid.viewmodel.RatesViewModel


class AccountDetailsFragment : Fragment() {

    companion object {
        val TAG: String? = "AccountDetailsFragment"
        fun newInstance() = AccountDetailsFragment()
    }

    private lateinit var textViewBalanceValue: TextView
    private lateinit var textViewEquityValue: TextView
    private lateinit var viewModel: RatesViewModel

    private var initBalance: Double = 10000.0

    private val renderRates = Observer<List<Rate>?> { rates ->


        /**
         * assumtion
         * account open and orginally have 10,000 USD worth units
         * B/E return the change or the value of each forex
         * balance simply calculate sum the total asset
         */
        val result = rates?.fold(0.0) { acc, rate ->
            var ret = acc
            rate.variation?.let {
                ret = acc + (initBalance + (initBalance * (it.div(100.0))))
            }
            ret
        }
        Log.v(TAG, "result $result")
        result?.let {
            textViewEquityValue.text = "$" + "%.2f".format(result)
        }

        var balance = rates?.count()?.times(initBalance)
        balance?.let {
            textViewBalanceValue.text = "$" + "%.2f".format(balance)
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var dataBinding = AccountdetailFragmentBinding.inflate(inflater, container, false)
        textViewEquityValue = dataBinding.textViewEquityValue
        textViewBalanceValue = dataBinding.textViewBalanceValue
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadRates()
        viewModel.startTimer()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(
            this,
            Injection.provideViewModelFactory()
        ).get(RatesViewModel::class.java)

        viewModel.liveRate.observe(viewLifecycleOwner, renderRates)
        Log.v(TAG, viewModel.toString())
    }

}