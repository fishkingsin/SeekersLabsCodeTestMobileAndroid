package hk.kingsin.seekerslabscodetestmobileandroid.data

interface OperationCallback<T> {
    fun onSuccess(data: T)
    fun onWarning(data: T, warningCode: Int)
    fun onError(error: String?)
}