package hk.kingsin.seekerslabscodetestmobileandroid.data

data class LiveRankingResponse (
        val supportedPairs: List<String>?,
        val message: String?,
        val code: Int
        ) {}