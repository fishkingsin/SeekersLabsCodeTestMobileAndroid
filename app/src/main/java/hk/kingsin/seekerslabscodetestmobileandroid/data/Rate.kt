package hk.kingsin.seekerslabscodetestmobileandroid.data

data class Rate (
        var name: String?,
        val rate: Double?,
        val timestamp: Long?,
        val sellPrice: Double?,
        val buyPrice: Double?,
        val variation: Double?
        ) {

}
