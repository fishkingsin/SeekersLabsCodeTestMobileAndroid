package hk.kingsin.seekerslabscodetestmobileandroid.data

import hk.kingsin.seekerslabscodetestmobileandroid.model.RateDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.ThreadLocalRandom


class RateRemoteDataSource(apiClient: ApiClient): RateDataSource {
    private var call: Call<RatesDataResponse>? = null
    private val service = ApiClient.build()

    val myJob = CoroutineScope(Dispatchers.IO).launch {

        withContext(Dispatchers.Main) {
            //do something with result
        }
    }

    override fun fetchRates(pairs: List<String>?, callback: OperationCallback<List<Rate>?>) {
        pairs?.let {
            call = if (pairs.isEmpty()) {
                service?.live()
            } else {
                val param = pairs?.joinToString().replace(" ", "")
                print(param)
                service?.rates(param)
            }

            call?.enqueue(object : Callback<RatesDataResponse> {
                override fun onFailure(call: Call<RatesDataResponse>, t: Throwable) {
                    callback.onError(t.message)
                }

                override fun onResponse(
                    call: Call<RatesDataResponse>,
                    response: Response<RatesDataResponse>
                ) {


                    /**
                     * convert api data to busniess date
                     * api response only have raw structure, code, array of pai
                     * what app needs is array of rate with corresponding data, e.g. change, buy/sell price,
                     */
                    response.body()?.let { itResponse ->
                        if (response.isSuccessful && (itResponse.code == 200)) {
                            callback.onSuccess(
                                itResponse.rates?.map {
                                    Rate(
                                        it.key,
                                        it.value.rate,
                                        it.value.timestamp,
                                        getVariation(DIRECTION.UP, it.value.rate),
                                        getVariation(DIRECTION.DOWN, it.value.rate),
                                        getVariationDelta()
                                    )
                                }
                            )
                        } else if (response.isSuccessful && (itResponse.code == 1001)) {
                            /** special handling for init stage, when there is no stored data,
                             * fetch data without query then catch the code to fetch real data
                            */
                            callback.onWarning(itResponse.supportedPairs?.map {
                                Rate(
                                    it,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null
                                )
                            }, itResponse.code)
                        } else {
                            callback.onError(itResponse.code.toString())
                        }
                    }
                }
            })
        }
    }
    enum class DIRECTION{
        UP, DOWN
    }
    fun getVariation(direction: DIRECTION, value: Double?): Double? {
        value?.let {
            val variation: Double = (ThreadLocalRandom.current().nextDouble(0.0,10.0) * 0.01) * value
            return when (direction) {
                DIRECTION.UP -> it?.plus(variation)
                DIRECTION.DOWN -> it?.minus(variation)
            }
        }
        return value
    }

    fun getVariationDelta(): Double? {
        return ThreadLocalRandom.current().nextDouble(-10.0,10.0)
    }

    override fun cancel() {
        call?.let {
            it.cancel()
        }
    }
}