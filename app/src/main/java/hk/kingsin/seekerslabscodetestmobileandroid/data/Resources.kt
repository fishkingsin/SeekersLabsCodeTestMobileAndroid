package hk.kingsin.seekerslabscodetestmobileandroid.data
import hk.kingsin.seekerslabscodetestmobileandroid.data.Status.ERROR
import hk.kingsin.seekerslabscodetestmobileandroid.data.Status.LOADING
import hk.kingsin.seekerslabscodetestmobileandroid.data.Status.SUCCESS

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): Resource<T> = Resource(status = Status.SUCCESS, data = data, message = null)

        fun <T> error(data: T?, message: String): Resource<T> =
            Resource(status = ERROR, data = data, message = message)

        fun <T> loading(data: T?): Resource<T> = Resource(status = LOADING, data = data, message = null)
    }
}