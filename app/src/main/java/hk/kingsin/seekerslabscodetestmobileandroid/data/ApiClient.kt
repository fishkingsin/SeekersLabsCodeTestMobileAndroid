package hk.kingsin.seekerslabscodetestmobileandroid.data

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

object ApiClient {

    //https://obscure-earth-55790.herokuapp.com/api/museums
    private const val API_BASE_URL = "https://www.freeforexapi.com/"

    private var servicesApiInterface: ServicesApiInterface? = null


    fun build(baseURL: String? = null): ServicesApiInterface? {

        var builder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(baseURL ?: API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

        var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.addInterceptor(interceptor())

        var retrofit: Retrofit = builder.client(httpClient.build()).build()
        servicesApiInterface = retrofit.create(
            ServicesApiInterface::class.java
        )

        return servicesApiInterface as ServicesApiInterface
    }

    private fun interceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    interface ServicesApiInterface {
        @GET("/api/live")
        fun live(): Call<RatesDataResponse>

        @GET("/api/live?")
        fun rates(@Query("pairs", encoded = true) pairs: String): Call<RatesDataResponse>

    }
}