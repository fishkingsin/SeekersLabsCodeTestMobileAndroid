package hk.kingsin.seekerslabscodetestmobileandroid.data

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}