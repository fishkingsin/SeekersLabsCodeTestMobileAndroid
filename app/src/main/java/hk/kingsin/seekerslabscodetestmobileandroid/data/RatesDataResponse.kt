package hk.kingsin.seekerslabscodetestmobileandroid.data

data class RatesDataResponse(
    val supportedPairs: List<String>?,
    val rates: Map<String, Rate>?,
    val message: String?,
    val code: Int?
) {

}