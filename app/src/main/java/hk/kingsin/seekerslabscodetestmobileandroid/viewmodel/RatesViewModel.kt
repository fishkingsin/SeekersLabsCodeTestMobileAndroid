package hk.kingsin.seekerslabscodetestmobileandroid.viewmodel

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hk.kingsin.seekerslabscodetestmobileandroid.data.OperationCallback
import hk.kingsin.seekerslabscodetestmobileandroid.data.Rate
import hk.kingsin.seekerslabscodetestmobileandroid.model.RateRepository
import kotlinx.coroutines.*
import javax.inject.Singleton


interface RatesViewModelInputs {
    fun loadRates()
    fun startTimer()
}

interface RatesViewModelOutputs {
    var liveRate: LiveData<List<Rate>?>
    val isViewLoading: LiveData<Boolean>
    val onMessageError: LiveData<Any>
    val isEmptyList: LiveData<Boolean>
}

class RatesViewModel(private val repository: RateRepository) : ViewModel(), RatesViewModelOutputs,
    RatesViewModelInputs {

    companion object {
        var instance: RatesViewModel? = null
    }

    fun getInstance(repository: RateRepository): RatesViewModel? {
        if (instance == null) {
            synchronized(Singleton::class.java) {
                if (instance == null) {
                    instance = RatesViewModel(repository)
                }
            }
        }
        return instance
    }

    private val job = Job()
    private var instance: RatesViewModel? = null

    public val outputs: RatesViewModelOutputs = this
    public val inputs: RatesViewModelInputs = this
    private val TAG: String = "RatesViewModel"
    private var allParis: List<String>? = null
    private val _rates = MutableLiveData<List<Rate>>().apply { value = emptyList() }
    private val rates: LiveData<List<Rate>> = _rates

    // helper function combine 2 live data
    private fun <T, K, R> LiveData<T>.combineWith(
        liveData: LiveData<K>,
        block: (T?, K?) -> R
    ): LiveData<R> {
        val result = MediatorLiveData<R>()
        result.addSource(this) {
            result.value = block(this.value, liveData.value)
        }
        result.addSource(liveData) {
            result.value = block(this.value, liveData.value)
        }
        return result
    }

    private val _prevRate = MutableLiveData<List<Rate>>().apply { value = emptyList() }
    private val prevRate: LiveData<List<Rate>> = _prevRate


    // combine old and new date and calculate the difference
    override var liveRate = rates.combineWith(prevRate) { rates, prevs ->
        rates?.map { rate ->
            rate.rate.let { r ->
                val prev = prevs?.find { prevPrice -> prevPrice.name == rate.name }
                prev?.let {
                    val variation = prev.rate?.let { it1 -> r?.minus(it1) }
                    Rate(
                        rate.name,
                        rate.rate,
                        rate.timestamp,
                        rate.sellPrice,
                        rate.buyPrice,
                        variation
                    )
                }
            }
            rate
        }
    }


    private val _isViewLoading = MutableLiveData<Boolean>()
    override val isViewLoading: LiveData<Boolean> = _isViewLoading

    private val _onMessageError = MutableLiveData<Any>()
    override val onMessageError: LiveData<Any> = _onMessageError

    private val _isEmptyList = MutableLiveData<Boolean>()
    override val isEmptyList: LiveData<Boolean> = _isEmptyList

    var mainHandler: Handler? = null

    private val updateTask = object : Runnable {
        override fun run() {
            loadRates()
            mainHandler?.postDelayed(this, 1000)
        }
    }

    var baseCurrency: String = "USD"

    private val coroutineScope = CoroutineScope(job + Dispatchers.Main)

    override fun startTimer() {
        if (mainHandler == null) {
            mainHandler = Handler(Looper.getMainLooper())
        }

        mainHandler?.post(updateTask)
    }

    fun _loadRates() {
        _isViewLoading.value = allParis.isNullOrEmpty()
        val data = allParis ?: emptyList<String>()
        repository.fetchRates(data, object : OperationCallback<List<Rate>?> {
            override fun onError(error: String?) {
                _isViewLoading.value = false
                error?.let {
                    _onMessageError.value = it
                }
            }

            override fun onSuccess(data: List<Rate>?) {
                _isViewLoading.value = false
                if (data.isNullOrEmpty()) {
                    _isEmptyList.value = true

                } else {
                    if (_prevRate.value.isNullOrEmpty()) {
                        _prevRate.value = data.filter { it.name!!.startsWith(baseCurrency) }
                    }
                    _rates.value = data.filter { it.name!!.startsWith(baseCurrency) }
                }
            }

            override fun onWarning(data: List<Rate>?, warningCode: Int) {
                _isViewLoading.value = true
                allParis = data?.map { it.name ?: "" }

                if (!allParis.isNullOrEmpty()) {
                    loadRates()
                }

            }
        })
    }

    // kick start the fetching
    override fun loadRates() {
        coroutineScope.launch {
            _loadRates()
        }
    }

    fun cancel() {
        repository.cancel()
        mainHandler?.removeCallbacks(updateTask)
        job.cancel()
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
        repository.cancel()
        mainHandler?.removeCallbacks(updateTask)
    }
}

