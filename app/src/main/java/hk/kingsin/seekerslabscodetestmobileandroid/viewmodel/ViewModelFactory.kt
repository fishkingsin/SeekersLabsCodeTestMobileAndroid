package hk.kingsin.seekerslabscodetestmobileandroid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import hk.kingsin.seekerslabscodetestmobileandroid.model.RateRepository

class ViewModelFactory (private val repository: RateRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RatesViewModel(repository) as T
    }
}