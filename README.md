# SeekersLabsCodeTestMobileAndroid
## Code test for seekers labs
# Developer Notes

- [x] Setup bitrise for pull request status checking
- [x] Create base MVVM classes
- [x] Create MVVM module
    - [x] Rate
    - [x] AccountInfo
- [x] Setup retrofit
- [x] Setup OKhttp
- [x] Create MainActivity with viewmodel
    - [x] integrate with MVVM callback and view model
- [x] Create UI component
    - [x] recycler adapter 
    - [x] recycler cell view
    - [x] table view fragment
    - [x] bottom tab navigator
    - [x] Theme and Style
- [x] UI + MVVM integration
- [x] Skeleton view animation before data is loaded
- [x] Unit Tests

# Recommand Enviroment

Before Running, developing and building the app, make sure your device's evniroment is suitable, The following specifications are recommonded:

* mocOS version >= 11.2.3
* android studio version >= 4.1.2
* android sdk version >= 9
* kotlin version >= 1.4 
* android device version >= 21 

## Build steps

1. clone `https://gitlab.com/fishkingsin/SeekersLabsCodeTestMobileAndroid.git`
1. open android studio 
1. import from existing project
1. run `app`
